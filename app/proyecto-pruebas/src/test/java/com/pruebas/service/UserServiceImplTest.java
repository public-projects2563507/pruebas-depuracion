package com.pruebas.service;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.pruebas.dto.UserDTO;
import com.pruebas.repository.IUserRepository;
import com.pruebas.service.impl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {


	private UserServiceImpl userService;
	
	@Mock
    private IUserRepository userRepository;
	
	@BeforeEach
    void setUp() {
		userService = new UserServiceImpl(userRepository);
    }
	
	@Test
	void whenCreateUser() throws Exception {
		
		UserDTO user = new UserDTO();
		user.setUserId(2);
		user.setDni(8888882);
		user.setFirstName("Name 2");
		user.setLastName1("Last Name 2-1");
		user.setLastName2("Last Name 2-2");
		
		userService.createUser(user);


	}
	
		
//	@Test
//	void whenGetUser() throws Exception {
//		
//		UserDTO user = new UserDTO();
//		user.setUserId(2);
//		user.setDni(8888882);
//		user.setFirstName("Name 2");
//		user.setLastName1("Last Name 2-1");
//		user.setLastName2("Last Name 2-2");
//		
//		userService.getUser(2);
//
//
//	}
	
	@Test
	void whenUpdateUser() throws Exception {
		
		UserDTO user = new UserDTO();
		user.setUserId(2);
		user.setDni(8888882);
		user.setFirstName("Name 2");
		user.setLastName1("Last Name 2-1");
		user.setLastName2("Last Name 2-2");
		
		userService.updateUser(user, user.getUserId());

	}

	
	
	@Test
	void whenGetAllUser() throws Exception {
		
		userService.getAllUsers();

	}
	
	@Test
	void whenDeleteUser() throws Exception {
		
		UserDTO user = new UserDTO();
		user.setUserId(2);
		user.setDni(8888882);
		user.setFirstName("Name 2");
		user.setLastName1("Last Name 2-1");
		user.setLastName2("Last Name 2-2");
		
		userService.deleteUser(user.getUserId());


	}

}
