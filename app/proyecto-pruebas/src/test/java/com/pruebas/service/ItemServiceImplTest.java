package com.pruebas.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.pruebas.dto.ItemDTO;
import com.pruebas.repository.IItemRepository;
import com.pruebas.service.impl.ItemServiceImpl;

@ExtendWith(MockitoExtension.class)
public class ItemServiceImplTest {


	private ItemServiceImpl itemService;
	
	@Mock
    private IItemRepository itemRepository;
	
	@BeforeEach
    void setUp() {
		itemService = new ItemServiceImpl(itemRepository);
    }
	
	@Test
	void whenGetAllItem() throws Exception {
				
		itemService.getAllItems();

	}
	
//	@Test
//	void whenGetItem() throws Exception {
//		
//		ItemDTO request = new ItemDTO();
//		request.setItemId(1);
//		request.setCode("001");
//		request.setTitle("title-001");
//		request.setName("name-001");
//		request.setDescription("description-001");
//		
//		itemService.getItem(1);
//
//
//	}
	
	@Test
	void whenUpdateItem() throws Exception {
		
		ItemDTO request = new ItemDTO();
		request.setItemId(1);
		request.setCode("001");
		request.setTitle("title-001");
		request.setName("name-001");
		request.setDescription("description-001");
		
		itemService.updateItem(request, request.getItemId());

	}

	@Test
	void whenCreateItem() throws Exception {
		
		ItemDTO request = new ItemDTO();
		request.setItemId(1);
		request.setCode("001");
		request.setTitle("title-001");
		request.setName("name-001");
		request.setDescription("description-001");
		
		itemService.createItem(request);


	}
	
	@Test
	void whenDeleteItem() throws Exception {
		
		ItemDTO request = new ItemDTO();
		request.setItemId(1);
		request.setCode("001");
		request.setTitle("title-001");
		request.setName("name-001");
		request.setDescription("description-001");
		
		itemService.deleteItem(request.getItemId());


	}
	
	@Test
	void whenGetTitle() throws Exception {

		itemService.getTitles(4.0);

	}
	
	@Test
	void whenGetAverageRatingLowerThan() throws Exception {

		itemService.getAverageRatingLowerThan(4.6);

	}

}
