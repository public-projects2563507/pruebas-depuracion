package com.pruebas.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.pruebas.dto.ItemDTO;
import com.pruebas.dto.ReviewDTO;
import com.pruebas.dto.UserDTO;
import com.pruebas.entity.Item;
import com.pruebas.entity.Review;
import com.pruebas.entity.User;
import com.pruebas.mapper.GenericResponse;
import com.pruebas.mapper.ReviewMapper;
import com.pruebas.repository.IReviewRepository;
import com.pruebas.service.impl.ReviewServiceImpl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ReviewServiceImplTest {

    @Mock
    private IReviewRepository reviewRepository;

    @InjectMocks
    private ReviewServiceImpl reviewService;

    @Captor
    private ArgumentCaptor<Review> reviewCaptor;

    @BeforeEach
    void setUp() {
        
    }

    @Test
    void whenGetAllReviews_thenReturnsAllReviews() {
        // Preparar el repositorio simulado para retornar una lista de Reviews
        Review review1 = new Review();
        
        Item item = new Item ();
        User user = new User ();
        item.setItemId(1);
        user.setUserId(1);
        
        review1.setReviewId(1);
        review1.setItem(item);
        review1.setUser(user);
        
        Review review2 = new Review();
        review2.setReviewId(2);
        item.setItemId(2);
        user.setUserId(2);
        review2.setItem(item);
        review2.setUser(user);
        
        when(reviewRepository.findAll()).thenReturn(List.of(review1, review2));

        // Llamar al método del servicio
        List<ReviewDTO> reviews = reviewService.getAllReviews();

        // Verificar que se llamó al método findAll() del repositorio
        verify(reviewRepository).findAll();

        // Verificar que se devuelven los DTOs esperados
        assertThat(reviews).hasSize(2);
        assertThat(reviews.get(0)).isEqualTo(ReviewMapper.getObjectDTO(review1));
        assertThat(reviews.get(1)).isEqualTo(ReviewMapper.getObjectDTO(review2));
    }

    @Test
    void whenGetReviewById_thenReturnsReviewDTO() {
        // Preparar el repositorio simulado para retornar un Optional con un Review
        Review review = new Review();
        Item item = new Item ();
        User user = new User ();
        item.setItemId(1);
        user.setUserId(1);
        
        review.setReviewId(1);
        review.setItem(item);
        review.setUser(user);
        
        when(reviewRepository.findById(eq(1))).thenReturn(Optional.of(review));

        // Llamar al método del servicio
        ReviewDTO reviewDTO = reviewService.getReview(1);

        // Verificar que se llamó al método findById() del repositorio
        verify(reviewRepository).findById(eq(1));

        // Verificar que se devuelve el DTO correcto
        assertThat(reviewDTO).isEqualTo(ReviewMapper.getObjectDTO(review));
    }

    @Test
    void whenDeleteReview_thenReturnsGenericResponse() {
        // Preparar el repositorio simulado para eliminar un Review por ID
        doNothing().when(reviewRepository).deleteById(any(Integer.class));

        // Llamar al método del servicio
        GenericResponse response = reviewService.deleteReview(1);

        // Verificar que se llamó al método deleteById() del repositorio
        verify(reviewRepository).deleteById(eq(1));

        // Verificar que se devuelve la respuesta correcta
        assertThat(response.getCode()).isEqualTo("00");
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getMessageResponse()).isEqualTo("object delete successful");
    }

    @Test
    void whenCreateReview_ThenReturnsGenericResponse() {
        // Preparar el repositorio simulado para guardar un nuevo Review
        ReviewDTO review = new ReviewDTO();
        
        review.setReviewId(1);
        review.setItemId(1);
        review.setUserId(1);
        
        when(reviewRepository.findById(eq(1))).thenReturn(Optional.empty());

        // Llamar al método del servicio
        GenericResponse response = reviewService.createReview(review);

        // Verificar que se llamó al método save() del repositorio
        verify(reviewRepository).save(any(Review.class));

        // Verificar que se devuelve la respuesta correcta
        assertThat(response.getCode()).isEqualTo("00");
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getMessageResponse()).isEqualTo("object creation successfull");
    }

    @Test
    void whenUpdateReview_ThenReturnsGenericResponse() {
        // Preparar el repositorio simulado para actualizar un Review
        Review review = new Review();
        review.setReviewId(1);
        when(reviewRepository.findById(eq(1))).thenReturn(Optional.of(review));

        // Llamar al método del servicio
        GenericResponse response = reviewService.updateReview(new ReviewDTO(), 1);

        // Verificar que se llamó al método save() del repositorio
        verify(reviewRepository).save(any(Review.class));

        // Verificar que se devuelve la respuesta correcta
        assertThat(response.getCode()).isEqualTo("00");
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getMessageResponse()).isEqualTo("object update successfull");
    }
}
