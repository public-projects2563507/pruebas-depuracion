package com.pruebas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.pruebas.controller.ItemController;
import com.pruebas.dto.ItemDTO;
import com.pruebas.service.impl.ItemServiceImpl;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.junit.jupiter.api.Test;

@AutoConfigureMockMvc
@ContextConfiguration(classes = { ItemController.class })
@WebMvcTest
class ItemControllerTest {

	@MockBean
	private ItemServiceImpl itemService;

	@Autowired
	private MockMvc mvc;

	@Test
	void whenGetAllItems() throws Exception {

		mvc.perform(get("/item-api/get-all-item").contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	void whenGetItem() throws Exception {
		
		mvc.perform(get("/item-api/get-item/" + 1).contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	void whenUpdateItem() throws Exception {

		ItemDTO request = new ItemDTO();
		request.setItemId(1);
		request.setCode("001");
		request.setTitle("title-001");
		request.setName("name-001");
		request.setDescription("description-001");
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(request);

		mvc.perform(put("/item-api/update-item/" + request.getItemId()).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(requestJson))
		.andExpect(status().isOk());

	}
	
	@Test
	void whenDeleteItem() throws Exception {
		mvc.perform(delete("/item-api/delete-item/" + 1).contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());

	}
	
	@Test
	void whenCreateItem() throws Exception {
		
		ItemDTO request = new ItemDTO();
		request.setItemId(1);
		request.setCode("001");
		request.setTitle("title-001");
		request.setName("name-001");
		request.setDescription("description-001");

		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(request);

		mvc.perform(post("/item-api/create-item").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(requestJson))
		.andExpect(status().isOk());

	}
	
	@Test
	void whenGetTitle() throws Exception {

		mvc.perform(get("/item-api/titles/" + 4.0).contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());

	}
	
	@Test
	void whenGetAverageRatingLowerThan() throws Exception {

		mvc.perform(get("/item-api/average-rating-lower-than/" + 4.6).contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());

	}

}
