package com.pruebas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.pruebas.controller.ReviewController;
import com.pruebas.dto.ReviewDTO;
import com.pruebas.service.impl.ReviewServiceImpl;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.junit.jupiter.api.Test;

@AutoConfigureMockMvc
@ContextConfiguration(classes = { ReviewController.class })
@WebMvcTest

class ReviewControllerTest {

	@MockBean
	private ReviewServiceImpl reviewService;

	@Autowired
	private MockMvc mvc;

	@Test
	void whenGetAllReviews() throws Exception {

		mvc.perform(get("/review-api/get-all-review").contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());

	}

	@Test
	void whenGetReview() throws Exception {
		
		mvc.perform(get("/review-api/get-review/" + 1).contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());
	}

	@Test
	void whenUpdateReview() throws Exception {

		ReviewDTO request = new ReviewDTO();
		request.setReviewId(1);
		request.setComment("comment-001-4.9");
		request.setRating(4.9);
		request.setItemId(1);
		request.setUserId(1);
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(request);

		mvc.perform(put("/review-api/update-review/" + request.getReviewId()).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(requestJson))
		.andExpect(status().isOk());

	}
	
	@Test
	void whenDeleteReview() throws Exception {
		mvc.perform(delete("/review-api/delete-review/" + 1).contentType(MediaType.APPLICATION_JSON_VALUE))
		.andExpect(status().isOk());

	}
	
	@Test
	void whenCreateReview() throws Exception {
		
		ReviewDTO request = new ReviewDTO();
		request.setReviewId(1);
		request.setComment("comment-001-4.9");
		request.setRating(4.9);
		request.setItemId(1);
		request.setUserId(1);

		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(request);

		mvc.perform(post("/review-api/create-review").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(requestJson))
		.andExpect(status().isOk());

	}

}
