package com.pruebas.mapper;

import java.util.ArrayList;
import java.util.List;

import com.pruebas.dto.ItemDTO;
import com.pruebas.entity.Item;

public class ItemMapper {

	public static ItemDTO getObjectDTO(Item item) {

		ItemDTO object = new ItemDTO();

		object.setItemId(item.getItemId());
		object.setCode(item.getCode());
		object.setTitle(item.getTitle());
		object.setName(item.getName());
		object.setDescription(item.getDescription());

		return object;
	}
	
	public static Item getObjectEntity(ItemDTO item) {

		Item object = new Item();

		object.setItemId(item.getItemId());
		object.setCode(item.getCode());
		object.setTitle(item.getTitle());
		object.setName(item.getName());
		object.setDescription(item.getDescription());

		return object;
	}

	public static List<ItemDTO> getListObject(List<Item> listItem) {

		List<ItemDTO> listObject = new ArrayList<ItemDTO>();
		ItemDTO object = null;

		for (Item item : listItem) {
			object = new ItemDTO();
			object.setItemId(item.getItemId());
			object.setCode(item.getCode());
			object.setTitle(item.getTitle());
			object.setName(item.getName());
			object.setDescription(item.getDescription());
			listObject.add(object);

		}

		return listObject;
	}

}
