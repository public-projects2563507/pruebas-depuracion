package com.pruebas.mapper;

import lombok.Data;

@Data
public class GenericResponse {

	private String code;
	private boolean success;
	private String messageResponse;
	private String technicalMessageResponse;

}
