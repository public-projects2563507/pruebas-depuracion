package com.pruebas.mapper;

import java.util.ArrayList;
import java.util.List;

import com.pruebas.dto.ReviewDTO;
import com.pruebas.entity.Item;
import com.pruebas.entity.Review;
import com.pruebas.entity.User;

public class ReviewMapper {

	public static ReviewDTO getObjectDTO(Review review) {

		ReviewDTO object = new ReviewDTO();

		object.setReviewId(review.getReviewId());
		object.setComment(review.getComment());
		object.setRating(review.getRating());
		object.setItemId(review.getItem().getItemId());
		object.setUserId(review.getUser().getUserId());

		return object;
	}

	public static Review getObjectEntity(ReviewDTO review) {

		Review object = new Review();
		Item item = new Item();
		User user = new User();
		item.setItemId(review.getItemId());
		user.setUserId(review.getUserId());

		object.setReviewId(review.getReviewId());
		object.setComment(review.getComment());
		object.setRating(review.getRating());
		object.setItem(item);
		object.setUser(user);

		return object;
	}

	public static List<ReviewDTO> getListObject(List<Review> listReview) {

		List<ReviewDTO> listObject = new ArrayList<ReviewDTO>();
		ReviewDTO object = null;

		for (Review review : listReview) {
			object = new ReviewDTO();

			object.setReviewId(review.getReviewId());
			object.setComment(review.getComment());
			object.setRating(review.getRating());
			object.setItemId(review.getItem().getItemId());
			object.setUserId(review.getUser().getUserId());

			listObject.add(object);

		}

		return listObject;
	}

}
