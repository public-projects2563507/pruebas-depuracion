package com.pruebas.mapper;

import java.util.ArrayList;
import java.util.List;

import com.pruebas.dto.UserDTO;
import com.pruebas.entity.User;

public class UserMapper {

	public static UserDTO getObjectDTO(User user) {

		UserDTO object = new UserDTO();

		object.setUserId(user.getUserId());
		object.setDni(user.getUserId());
		object.setFirstName(user.getFirstName());
		object.setSecondName(user.getSecondName());
		object.setLastName1(user.getLastName1());
		object.setLastName2(user.getLastName2());

		return object;
	}
	
	public static User getObjectEntity(UserDTO user) {

		User object = new User();

		object.setUserId(user.getUserId());
		object.setDni(user.getUserId());
		object.setFirstName(user.getFirstName());
		object.setSecondName(user.getSecondName());
		object.setLastName1(user.getLastName1());
		object.setLastName2(user.getLastName2());

		return object;
	}
	
	public static List<UserDTO> getListObject(List<User> listUser) {

		List<UserDTO> listObject = new ArrayList<UserDTO>();
		UserDTO object = null;

		for (User user : listUser) {
			object = new UserDTO();
			object.setUserId(user.getUserId());
			object.setDni(user.getUserId());
			object.setFirstName(user.getFirstName());
			object.setSecondName(user.getSecondName());
			object.setLastName1(user.getLastName1());
			object.setLastName2(user.getLastName2());

			listObject.add(object);

		}

		return listObject;
	}

}
