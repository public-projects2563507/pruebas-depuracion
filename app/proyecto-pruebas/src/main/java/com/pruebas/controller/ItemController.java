package com.pruebas.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import com.pruebas.dto.ItemDTO;
import com.pruebas.mapper.AverageRatingLowerThanResponse;
import com.pruebas.mapper.GenericResponse;
import com.pruebas.service.IItemService;

@RestController
@RequestMapping("/item-api")
public class ItemController {

	private final IItemService itemService;

	public ItemController(IItemService itemService) {
		this.itemService = itemService;
	}

	@GetMapping(value = {
			"/get-all-item" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ItemDTO>> getAllItems() throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(itemService.getAllItems());
	}

	@GetMapping(value = {
			"/get-item/{id}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ItemDTO> getItem(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(itemService.getItem(id));
	}

	@DeleteMapping(value = {
			"/delete-item/{id}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> deleteItem(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(itemService.deleteItem(id));
	}

	@PostMapping(value = {
			"/create-item" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> createItem(@RequestBody ItemDTO item) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(itemService.createItem(item));
	}

	@PutMapping(value = { "/update-item/{id}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> updateItem(@RequestBody ItemDTO item, @PathVariable Integer id)
			throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(itemService.updateItem(item, id));
	}

	@GetMapping(value = { "/titles/{rating}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> getTitles(@PathVariable Double rating) throws Exception {
		return ResponseEntity.status(HttpStatus.OK).body(itemService.getTitles(rating));
	}

	@GetMapping(value = {
			"/average-rating-lower-than/{rating}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AverageRatingLowerThanResponse> getAverageRatingLowerThan(@PathVariable Double rating) throws Exception {
		return ResponseEntity.status(HttpStatus.OK).body(itemService.getAverageRatingLowerThan(rating));
	}

}
