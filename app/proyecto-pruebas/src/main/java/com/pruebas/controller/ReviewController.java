package com.pruebas.controller;

import java.util.List;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;

import com.pruebas.dto.ReviewDTO;
import com.pruebas.mapper.GenericResponse;
import com.pruebas.service.IReviewService;

@RestController
@RequestMapping("/review-api")
public class ReviewController {

	private final IReviewService reviewService;

	public ReviewController(IReviewService reviewService) {
		this.reviewService = reviewService;
	}

	@GetMapping(value = {
			"/get-all-review" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ReviewDTO>> getAllReviews() throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(reviewService.getAllReviews());
	}

	@GetMapping(value = {
			"/get-review/{id}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ReviewDTO> getReview(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(reviewService.getReview(id));
	}

	@DeleteMapping(value = {
			"/delete-review/{id}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> deleteReview(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(reviewService.deleteReview(id));
	}

	@PostMapping(value = {
			"/create-review" },  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> createReview(@RequestBody ReviewDTO review) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(reviewService.createReview(review));
	}

	@PutMapping(value = { "/update-review/{id}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> updateReview(@RequestBody ReviewDTO review, @PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(reviewService.updateReview(review, id));
	}

}
