package com.pruebas.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

import com.pruebas.dto.UserDTO;
import com.pruebas.mapper.GenericResponse;
import com.pruebas.service.IUserService;

@RestController
@RequestMapping("/user-api")
public class UserController {

	private final IUserService userService;

	public UserController(IUserService userService) {
		this.userService = userService;
	}

	@GetMapping(value = {
			"/get-all-user" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDTO>> getAllUsers() throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(userService.getAllUsers());
	}

	@GetMapping(value = {
			"/get-user/{id}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> getUser(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(userService.getUser(id));
	}

	@DeleteMapping(value = {
			"/delete-user/{id}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> deleteUser(@PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(userService.deleteUser(id));
	}

	@PostMapping(value = {
			"/create-user" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> createUser(@RequestBody UserDTO user) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(userService.createUser(user));
	}

	@PutMapping(value = { "/update-user/{id}" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GenericResponse> updateUser(@RequestBody UserDTO user, @PathVariable Integer id) throws Exception {

		return ResponseEntity.status(HttpStatus.OK).body(userService.updateUser(user, id));
	}

}
