package com.pruebas.dto;


import lombok.Data;

@Data
public class ItemDTO {
	
	private Integer itemId;
	
	private String code;
	
	private String title;
	
	private String name;
	
	private String description;

}
