package com.pruebas.dto;

import lombok.Data;

@Data
public class ReviewDTO {
	
	private Integer reviewId;

	private String comment;

	private Double rating;
	
	private Integer itemId;

	private Integer userId;

}
