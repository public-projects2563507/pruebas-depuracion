package com.pruebas.dto;

import lombok.Data;

@Data
public class UserDTO {
	
	private Integer userId;
	
	private Integer dni;
	
	private String firstName;
	
	private String secondName;
	
	private String lastName1;
	
	private String lastName2;
	

}
