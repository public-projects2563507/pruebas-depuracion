package com.pruebas.service.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

import com.pruebas.dto.ReviewDTO;
import com.pruebas.entity.Review;
import com.pruebas.mapper.GenericResponse;
import com.pruebas.mapper.ReviewMapper;
import com.pruebas.repository.IReviewRepository;
import com.pruebas.service.IReviewService;

@Service
public class ReviewServiceImpl implements IReviewService {

	private IReviewRepository reviewRepository;

	public ReviewServiceImpl(IReviewRepository reviewRepository) {
		this.reviewRepository = reviewRepository;
	}

	@Override
	public List<ReviewDTO> getAllReviews() {

		return ReviewMapper.getListObject(reviewRepository.findAll());
	}

	@Override
	public ReviewDTO getReview(Integer id) {

		ReviewDTO reviewDTO = null;
		Optional<Review> object = reviewRepository.findById(id);

		if (object.isPresent()) {
			reviewDTO = ReviewMapper.getObjectDTO(object.get());
		}

		return reviewDTO;

	}

	@Override
	public GenericResponse deleteReview(Integer id) {

		GenericResponse response = new GenericResponse();
		reviewRepository.deleteById(id);

		response.setCode("00");
		response.setSuccess(true);
		response.setMessageResponse("object delete successfull");
		response.setTechnicalMessageResponse("OK");

		return response;
	}

	@Override
	public GenericResponse createReview(ReviewDTO review) {

		GenericResponse response = new GenericResponse();

		Optional<Review> object = reviewRepository.findById(review.getReviewId());

		if (object.isEmpty()) {

			reviewRepository.save(ReviewMapper.getObjectEntity(review));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object creation successfull");
			response.setTechnicalMessageResponse("OK");

		} else {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object already exist with id " + review.getReviewId());
			response.setTechnicalMessageResponse("FAIL");
		}

		return response;

	}

	@Override
	public GenericResponse updateReview(ReviewDTO review, Integer id) {

		GenericResponse response = new GenericResponse();

		Optional<Review> object = reviewRepository.findById(id);

		if (object.isEmpty()) {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object not exist");
			response.setTechnicalMessageResponse("FAIL");
		} else {
			reviewRepository.save(ReviewMapper.getObjectEntity(review));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object update successfull");
			response.setTechnicalMessageResponse("OK");
		}

		return response;

	}

}
