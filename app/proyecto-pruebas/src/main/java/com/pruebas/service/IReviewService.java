package com.pruebas.service;

import java.util.List;

import com.pruebas.dto.ReviewDTO;
import com.pruebas.mapper.GenericResponse;

public interface IReviewService {

	public List<ReviewDTO> getAllReviews();
	
	public ReviewDTO getReview(Integer id);
	
	public GenericResponse deleteReview(Integer id);
	
	public GenericResponse createReview(ReviewDTO review);

	public GenericResponse updateReview(ReviewDTO review, Integer id);

}
