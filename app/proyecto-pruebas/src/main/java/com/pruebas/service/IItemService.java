package com.pruebas.service;

import java.util.List;

import com.pruebas.dto.ItemDTO;
import com.pruebas.mapper.AverageRatingLowerThanResponse;
import com.pruebas.mapper.GenericResponse;

public interface IItemService {

	public List<ItemDTO> getAllItems();

	public ItemDTO getItem(Integer id);

	public GenericResponse deleteItem(Integer id);

	public GenericResponse createItem(ItemDTO review);

	public GenericResponse updateItem(ItemDTO review, Integer id);

	public List<String> getTitles(Double rating);

	public AverageRatingLowerThanResponse getAverageRatingLowerThan(Double rating);

}
