package com.pruebas.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.pruebas.dto.UserDTO;
import com.pruebas.entity.User;
import com.pruebas.mapper.GenericResponse;
import com.pruebas.mapper.UserMapper;
import com.pruebas.repository.IUserRepository;
import com.pruebas.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	private IUserRepository userRepository;

	public UserServiceImpl(IUserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public List<UserDTO> getAllUsers() {

		return UserMapper.getListObject(userRepository.findAll());
	}

	@Override
	public UserDTO getUser(Integer id) {

		UserDTO userDTO = null;
		Optional<User> object = userRepository.findById(id);

		if (object.isPresent()) {
			userDTO = UserMapper.getObjectDTO(object.get());
		}

		return userDTO;

	}

	@Override
	public GenericResponse deleteUser(Integer id) {

		GenericResponse response = new GenericResponse();
		userRepository.deleteById(id);

		response.setCode("00");
		response.setSuccess(true);
		response.setMessageResponse("object delete successfull");
		response.setTechnicalMessageResponse("OK");

		return response;
	}

	@Override
	public GenericResponse createUser(UserDTO user) {

		GenericResponse response = new GenericResponse();

		Optional<User> object = userRepository.findById(user.getUserId());

		if (object.isEmpty()) {

			userRepository.save(UserMapper.getObjectEntity(user));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object create successfull");
			response.setTechnicalMessageResponse("OK");

		} else {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object already exist with id " + user.getUserId());
			response.setTechnicalMessageResponse("FAIL");
		}

		return response;

	}

	@Override
	public GenericResponse updateUser(UserDTO user, Integer id) {

		GenericResponse response = new GenericResponse();

		Optional<User> object = userRepository.findById(id);

		if (object.isEmpty()) {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object not exist");
			response.setTechnicalMessageResponse("FAIL");
		} else {
			userRepository.save(UserMapper.getObjectEntity(user));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object update successfull");
			response.setTechnicalMessageResponse("OK");
		}

		return response;

	}

}
