package com.pruebas.service;

import java.util.List;

import com.pruebas.dto.UserDTO;
import com.pruebas.mapper.GenericResponse;

public interface IUserService {

	public List<UserDTO> getAllUsers();
	
	public UserDTO getUser(Integer id);
	
	public GenericResponse deleteUser(Integer id);
	
	public GenericResponse createUser(UserDTO user);

	public GenericResponse updateUser(UserDTO user, Integer id);

}
