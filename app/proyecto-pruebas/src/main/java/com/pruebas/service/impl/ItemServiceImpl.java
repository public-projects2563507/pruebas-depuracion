package com.pruebas.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.pruebas.dto.ItemDTO;
import com.pruebas.entity.Item;
import com.pruebas.mapper.AverageRatingLowerThanResponse;
import com.pruebas.mapper.GenericResponse;
import com.pruebas.mapper.ItemMapper;
import com.pruebas.repository.IItemRepository;
import com.pruebas.service.IItemService;

@Service
public class ItemServiceImpl implements IItemService {

	private IItemRepository itemRepository;

	public ItemServiceImpl(IItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}

	@Override
	public List<ItemDTO> getAllItems() {

		return ItemMapper.getListObject(itemRepository.findAll());
	}

	@Override
	public ItemDTO getItem(Integer id) {

		ItemDTO itemDTO = null;
		Optional<Item> object = itemRepository.findById(id);
		
		if(object.isPresent()) {
			
			itemDTO = ItemMapper.getObjectDTO(object.get());
		}
		
		return itemDTO;

	}

	@Override
	public GenericResponse deleteItem(Integer id) {

		GenericResponse response = new GenericResponse();
		itemRepository.deleteById(id);

		response.setCode("00");
		response.setSuccess(true);
		response.setMessageResponse("object delete successfull");
		response.setTechnicalMessageResponse("OK");

		return response;
	}

	@Override
	public GenericResponse createItem(ItemDTO item) {

		GenericResponse response = new GenericResponse();

		Optional<Item> object = itemRepository.findById(item.getItemId());

		if (object.isEmpty()) {

			itemRepository.save(ItemMapper.getObjectEntity(item));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object update successfull");
			response.setTechnicalMessageResponse("OK");

		} else {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object already exist with id " + item.getItemId());
			response.setTechnicalMessageResponse("FAIL");
		}

		return response;

	}

	@Override
	public GenericResponse updateItem(ItemDTO item, Integer id) {

		GenericResponse response = new GenericResponse();

		Optional<Item> object = itemRepository.findById(id);

		if (object.isEmpty()) {
			response.setCode("01");
			response.setSuccess(false);
			response.setMessageResponse("the object not exist");
			response.setTechnicalMessageResponse("FAIL");
		} else {
			itemRepository.save(ItemMapper.getObjectEntity(item));
			response.setCode("00");
			response.setSuccess(true);
			response.setMessageResponse("object update successfull");
			response.setTechnicalMessageResponse("OK");
		}

		return response;

	}

	@Override
	public List<String> getTitles(Double rating) {

		return itemRepository.findTitlesItemsByRating(rating);

	}

	@Override
	public AverageRatingLowerThanResponse getAverageRatingLowerThan(Double rating) {

		Double average = 0D;
		AverageRatingLowerThanResponse response = new AverageRatingLowerThanResponse();
		List<Item> listItem = itemRepository.findItemsWithAverageRatingLowerThan(rating);
		List<Double> listRating = new ArrayList<>();

		response.setSuccess(true);
		response.setCode("00");
		response.setMessageResponse("Average succesfull");

		if (listItem.size() > 0) {

			listItem = listItem.stream().distinct().collect(Collectors.toList());

			for (Item item : listItem) {
				item.getReviews().stream()
						.filter(filterItemId -> filterItemId.getItem().getItemId() == item.getItemId())
						.filter(filterRating -> filterRating.getRating() < rating)
						.map(mapRating -> mapRating.getRating())
						.forEach(valueRating -> listRating.add(valueRating));

			}

			average = listRating.stream().reduce((double) 0, Double::sum) / listRating.stream().count();
			response.setTechnicalMessageResponse("Exist rating");
			
		} else {
			response.setTechnicalMessageResponse("Not exist rating");
		}

		response.setAverageRating(average);

		return response;

	}

}
