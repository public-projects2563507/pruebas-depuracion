package com.pruebas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pruebas.entity.User;


public interface IUserRepository extends JpaRepository<User, Integer>{

}
