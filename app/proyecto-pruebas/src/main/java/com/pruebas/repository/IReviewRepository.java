package com.pruebas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pruebas.entity.Review;


public interface IReviewRepository extends JpaRepository<Review, Integer>{

}
