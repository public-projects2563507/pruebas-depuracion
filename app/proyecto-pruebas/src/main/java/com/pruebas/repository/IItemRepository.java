package com.pruebas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pruebas.entity.Item;

public interface IItemRepository extends JpaRepository<Item, Integer> {

	@Query("select tbl_item.title from Item tbl_item inner "
			+ "join Review tbl_review on tbl_item.id = tbl_review.item "
			+ "where tbl_review.rating = :rating")
	List<String> findTitlesItemsByRating(@Param("rating") Double rating);

	@Query("select tbl_item from Item tbl_item inner "
			+ "join Review tbl_review on tbl_item.id = tbl_review.item "
			+ "where tbl_review.rating < :rating")
	List<Item> findItemsWithAverageRatingLowerThan(@Param("rating") Double rating);

}
