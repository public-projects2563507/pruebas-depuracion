package com.pruebas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "review")
@Getter
@Setter
public class Review {

	@Id
	@Column(name = "review_id")
	private Integer reviewId;

	@Column(name = "comment")
	private String comment;

	@Column(name = "rating")
	private Double rating;

	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id")
	private Item item;	

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "user_id")	
	private User user;
	
	

}
