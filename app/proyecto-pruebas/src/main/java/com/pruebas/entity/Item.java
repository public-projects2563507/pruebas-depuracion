package com.pruebas.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "item")
@Getter
@Setter
public class Item {

	@Id
	@Column(name = "item_id")
	private Integer itemId;

	@Column(name = "code")
	private String code;

	@Column(name = "title")
	private String title;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@OneToMany(mappedBy="item")
	private List<Review> reviews;


}
