CREATE TABLE `item` (
  `item_id` int(11) NOT NULL,
  `code` varchar(45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `second_name` varchar(45) DEFAULT NULL,
  `last_name_1` varchar(45) NOT NULL,
  `last_name_2` varchar(45) DEFAULT NULL,
  `dni` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `review` (
  `review_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` double NOT NULL,
  `comment` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`review_id`),
  KEY `fk_review_item_idx` (`item_id`),
  KEY `fk_review_user_idx` (`user_id`),
  CONSTRAINT `fk_review_item` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_review_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into item (item_id, code, title, name, description) values (1, 'title-001', '001', 'description-001');
insert into item (item_id, code, title, name, description) values (2, 'title-002', '002', 'description-002');
insert into item (item_id, code, title, name, description) values (3, 'title-003', '003', 'description-003');
insert into item (item_id, code, title, name, description) values (4, 'title-004', '004', 'description-004');
insert into item (item_id, code, title, name, description) values (5, 'title-005', '005', 'description-005');

insert into user (user_id, first_name, second_name, last_name_1, last_name_2, dni) values (1, 'admin', 'admin', 'admin', 'admin', 1);
insert into user (user_id, first_name, second_name, last_name_1, last_name_2, dni) values (2, 'admin2', 'admin2', 'admin2', 'admin2', 2);

insert into review (review_id, item_id, user_id, rating, comment) values (1,1, 1, 4.9, 'comment-001-4.9');
insert into review (review_id, item_id, user_id, rating, comment) values (2,1, 1, 4.6, 'comment-001-4.6');
insert into review (review_id, item_id, user_id, rating, comment) values (3,1, 1, 4.1, 'comment-001-4.1');
insert into review (review_id, item_id, user_id, rating, comment) values (4,2, 1, 4.5, 'comment-001-4.5');
insert into review (review_id, item_id, user_id, rating, comment) values (5,2, 1, 4, 'comment-001-4.0');
insert into review (review_id, item_id, user_id, rating, comment) values (6,1, 1, 4, 'comment-001-4.0');
insert into review (review_id, item_id, user_id, rating, comment) values (7,1, 1, 4.7, 'comment-001-4.7');