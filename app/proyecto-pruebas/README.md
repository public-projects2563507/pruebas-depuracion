# challenge-bcp



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/public-projects2563507/challenge-bcp.git
git branch -M main
git push -uf origin main
```

Se implementa microservicio en el puerto 8001 con JDK 11 y Spring boot 2.7.8 usando gralde como gestor de repositorio de objetos.
Se encuentran los servicios CRUD de las entidades item, review y usuario ademas los dos servicios getTitles y getAverageRatingLowerThan
en el proyecto se contemplo incluir documentacion swagger 2 y colleccion de postman

Imagen Swagger:
![imagen-swagger](/docs/swagger.png)

Imagen Colleccion de postman:
![imagen-postman](/docs/postman.png)

Imagen Scripts Tablas:
![imagen-scripts](/docs/scripts.png)

Imagen Unit Test:
![imagen-scripts](/docs/test.png)
